import { NativeModules, DeviceEventEmitter } from 'react-native';


export default class ServerSocketModule {

    ServerSocketMod = NativeModules.ServerSocketModule;

    constructor (props) {

        this.addDeviceListeners()

    }

    _dListeners = {};

    setupServerSocket(address, serverPort) {
        this.ServerSocketMod.setupServerSocket(address, serverPort);
    }

    startThread() {
        this.ServerSocketMod.startThread();
    }


    addDeviceListeners (){
        if (Object.keys(this._dListeners).length){
            return this.emit('error', new Error("Socket listeners are already in place"))
        }

        this._dListeners.ReceivedMessage = DeviceEventEmitter.addListener('ReceivedMessage', params => {
            console.log(params)
        });

        this._dListeners.ThreadStarted = DeviceEventEmitter.addListener('ThreadStarted', params => {
            console.log(params)
        });

        this._dListeners.MessageSetted = DeviceEventEmitter.addListener('MessageSetted', params => {
            console.log(params)
        });
      

    }

}
