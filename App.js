/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Keyboard, Platform, StyleSheet, Text, TextInput, View, Button, NativeModules} from 'react-native';
import ServerSocketModule from './ServerSocketModule';
const ServerSocketMod =  new ServerSocketModule();
import SocketModule from './SocketModule';
const SocketMod =  new SocketModule();
import WiFiDirect from './WiFiDirect.js'
const wifidirectmod = new WiFiDirect();


const serverPort = 19001;

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component {
  state = {
    ip: ' '
  }

  handleIP = (text) => {
    this.setState({ ip: text })
  }

  connectToServer = () => {
    console.log(this.state.ip)
    SocketMod.setupSocket(this.state.ip, serverPort)
    SocketMod.startThread();
  }

  startListening = () => {
    ServerSocketMod.setupServerSocket(this.state.ip, serverPort);
    ServerSocketMod.startThread();
    console.log("Server has been setup")
  }

  registerService = () => {
    wifidirectmod.registerService("_presence._tcp", "alexandre")
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Button
          onPress={this.startListening}
          title="Setup Server"
          color="red"
          accessibilityLabel="Setup a Socket Server"
        />
        <Button
          onPress={this.connectToServer}
          title="Connect with Server"
          color="green"
          accessibilityLabel="Setup a connection with the Socket Server"
        />
        <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "IP"
               placeholderTextColor = "#9a73ef"
               autoCapitalize = "none"
               keyboardType = 'numbers-and-punctuation'
               onChangeText = {this.handleIP}
               />
        <Button
          onPress={sendHello}
          title="Send Hello"
          color="orange"
        />
        <Button
          onPress={buttonClicked}
          title="Connect with Server"
          color="green"
          accessibilityLabel="Setup a connection with the Socket Server"
        />
          <Button onPress={wifidirectmod.initWifiDirect}
          title="Start init"
          color="#841584"
          />
          <Button onPress={wifidirectmod.discoverPeers}
                  title="Start Peer Discovery"
                  color="#841584"
          />
          <Button onPress={this.registerService}
                  title="Start registration"
                  color="#841584"
          />
          <Button onPress={wifidirectmod.discoverServices}
                  title="Start Service Discovery"
                  color="#841584"
          />
      </View>
    );
  }
}
function buttonClicked(){
  console.log(NativeModules)
}


function sendHello(){
  SocketMod.setMessage("Hello")
  console.log("Hello has been sent")
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
