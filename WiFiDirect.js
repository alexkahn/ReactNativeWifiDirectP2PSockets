import {DeviceEventEmitter, NativeModules} from 'react-native';
import ServerSocketModule from "./ServerSocketModule";
import SocketModule from "./SocketModule";


const WiFiDirect = NativeModules.WiFiDirectModule;

var devices = [];
var services= [];

var watchfulltype = "";
var watchflag = false;



function ipToGOIP(ip) {
    arr = ip.split(".");
    var ip = arr[0] + "." + arr[1] + "." + arr[2] + ".1";
    console.log(ip);
    return ip.toString();
    
}

var connectTo;

var action;
var service = null;
var name;
var address;

var connectcallback = null;

var devicename = ""; // For debugging purposes



export default class WiFiDirectModule {

    watchcallback(params, service) {
        if (params.groupOwner){
            serversocket = new ServerSocketModule;
            ipaddress = params.ownAddress;
            serversocket.setupServerSocket(ipaddress, 19000);
            serversocket.startThread();
        }
        else {
            socket = new SocketModule;
            socket.setupSocket(params.otherDevicesAddress, 19000);
            socket.startThread();
            socket.setMessage("Hello we are connected over WifiDirect")
        }
    
    }


    constructor (props) {

        this._dListeners = {};

        this.addDeviceListeners();

        this.groupowner;
    }


    addDeviceListeners (){
        if (Object.keys(this._dListeners).length){
            return this.emit('error', new Error("WiFi-Direct listeners are already in place"))
        }

        this._dListeners.onWifiDirectPeers = DeviceEventEmitter.addListener('onWifiDirectPeers', params => {
            if (!devices.includes(params)) {
                console.log(devices);                devices.push(params);

            }
        });

        this._dListeners.onWifiDirectServices = DeviceEventEmitter.addListener('onWifiDirectServices', params => {
                console.log("Services: ")
                console.log(services);
                services.push(params);
                if (watchflag) {
                    if (watchfulltype === params.ServiceType) {

                        connectTo = params;
                        this.connectService(params.MacAddress)
                    }
                }
        });
        this._dListeners.onWifiDirectConnected = DeviceEventEmitter.addListener('onWifiDirectConnected', params => {
            console.log(params);
            if (watchflag) {
                console.log(services);
                console.log("Doing callback")
                this.watchcallback(params, service);
            }     
        });
    }

    /**
     * Remove all event listeners and clean map
     */
    removeDeviceListeners () {
        Object.keys(this._dListeners).forEach(name => this._dListeners[name].remove())
        this._dListeners = {}
    }

    initWifiDirect(){
        console.log("Clicked init");
        WiFiDirect.initWifiDirect()
    }
    registerService(serviceType, name){
        WiFiDirect.startRegistration(name, serviceType);
        this.watch(serviceType, this.cllback());
        this.discoverServices();
    }
    cllback(){
        console.log("Connected")
    }
    discoverPeers(){
        devices= [];
        console.log("Clicked Discover peers");
        WiFiDirect.discoverPeers();
    }
    discoverServices(){
        services = [];
        console.log("Clicked Discover services");
        WiFiDirect.discoverServices();
    }
    connectService(address){
        console.log("Connect to " + address);
        WiFiDirect.wifiDirectConnectToPeer(address);
    }

    register(type, domain, name, port, txtRecord){
        this.devicename = name;
        this.registerService(type, port, name, txtRecord)
    }
    unregister(type, name){
        WiFiDirect.deleteRegistration(name);
    }
    watch(fulltype, succes) {
        watchflag = true;
        watchfulltype = fulltype +".local.";
        watchcallback = succes;
    }
    unwatch(type, domain, success, failure){
        watchflag = false;
        watchcallback = null;
    }


    getDevices(){
        return devices;
    }
    getServices(){
        return services;
    }






}
