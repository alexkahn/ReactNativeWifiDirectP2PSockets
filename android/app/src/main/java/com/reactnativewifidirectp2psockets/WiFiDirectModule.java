package com.reactnativewifidirectp2psockets;


import android.app.Activity;
import android.content.*;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.os.AsyncTask;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import com.reactnativewifidirectp2psockets.WifiDirectSocketAsyncTask;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;

import java.net.*;
import java.util.*;

import static android.content.ContentValues.TAG;

/**
 * Created by alexandre on 25/02/2018.
 * Copied and adapted from https://github.com/winaacc/sport_native/
 */

public class WiFiDirectModule extends ReactContextBaseJavaModule implements LifecycleEventListener {
    public WiFiDirectModule(ReactApplicationContext reactContext){
        super(reactContext);
        context = reactContext;
        reactContext.addLifecycleEventListener(this);
        wifiP2pManager = (WifiP2pManager) reactContext.getSystemService(Context.WIFI_P2P_SERVICE);
    }

    public void sendEvent(String eventName, @Nullable WritableMap params){
        context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName,params);
    }

    protected ReactApplicationContext context;
    private WifiP2pInfo mWifiP2pInfo;
    private WifiP2pManager wifiP2pManager;
    private WifiManager wifiManager;
    private WifiP2pManager.Channel wifiDirectChannel;
    private WifiP2pDnsSdServiceRequest serviceRequest;
    private int port =  8888;

    private Set services = new HashSet<>();
    private Set devices = new HashSet<>();

    // Toast for debugging
    int duration = Toast.LENGTH_SHORT;

    // Keep a HashMap with all the registered services
    private HashMap<String, WifiP2pDnsSdServiceInfo> RegisteredServices = new HashMap();





    private WifiP2pManager.ConnectionInfoListener mInfoListener = new WifiP2pManager.ConnectionInfoListener(){
        @Override
        public void onConnectionInfoAvailable(final WifiP2pInfo info) {

            String groupOwnerAddress = info.groupOwnerAddress.getHostAddress();

            // After the group negotiation, we can determine the group owner
            // (server).
            if (info.groupFormed) {
                // Do whatever tasks are specific to the group owner.
                // One common case is creating a group owner thread and accepting
                // incoming connections.

                CharSequence txt;
                WritableMap groupOwnerParams = Arguments.createMap();
                groupOwnerParams.putBoolean("groupOwner", info.isGroupOwner);
                if(info.isGroupOwner){
                    txt = "Group Owner";
                    ServerSocket serverSocket = null;

                    try {
                        serverSocket = new ServerSocket(port);
                        serverSocket.setReuseAddress(true);
                        Socket client = serverSocket.accept();
                        ObjectInputStream objectInputStream = new ObjectInputStream(client.getInputStream());
                        Object object = objectInputStream.readObject();
                        if (object.getClass().equals(String.class) && ((String) object).equals("AMBIENTJS")) {
                            Log.d(TAG, "Client IP address: " + client.getInetAddress());
                            String address = client.getInetAddress().getHostAddress();
                            groupOwnerParams.putString("otherDevicesAddress", address);
                            groupOwnerParams.putString("ownAddress", groupOwnerAddress);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }

                }
                else{
                    groupOwnerParams.putString("otherDevicesAddress", groupOwnerAddress);

                    txt = "Not GO";
                    Socket socket = new Socket();

                    try {
                        socket.setReuseAddress(true);
                        socket.connect((new InetSocketAddress(groupOwnerAddress, port)), 5000);
                        String ownAddress = socket.getLocalAddress().getHostName();
                        groupOwnerParams.putString("ownAddress", ownAddress);
                        OutputStream os = socket.getOutputStream();
                        ObjectOutputStream oos = new ObjectOutputStream(os);
                        oos.writeObject(new String("AMBIENTJS"));
                        oos.close();
                        os.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                sendEvent("onWifiDirectConnected", groupOwnerParams);
                Toast.makeText(context, txt, duration).show();

            }
        }
    };

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)){
                wifiP2pManager.requestPeers(wifiDirectChannel,mPeerListListener);
            }else if(WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION.equals(action)){

            }else if(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)){
                NetworkInfo networkInfo = (NetworkInfo)intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
                if(networkInfo.isConnected()){
                    Log.i("WiFi-Direct", "Connected with succes");
                    wifiP2pManager.requestConnectionInfo(wifiDirectChannel,mInfoListener);
                }else{
                    Log.i("WiFi-Direct", "Failed connection");
                }
            }
        }
    };

    @Override
    public String getName(){
        return "WiFiDirectModule";
    }

    @Override
    public void onHostResume(){
        wifiDirectChannel = wifiP2pManager.initialize(context, Looper.myLooper(),null);
        IntentFilter mFilter = new IntentFilter();
        mFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mFilter.addAction(WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION);
        mFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
        context.registerReceiver(broadcastReceiver, mFilter);
    }

    @Override
    public void onHostPause(){
        context.unregisterReceiver(broadcastReceiver);
        wifiP2pManager.removeGroup(wifiDirectChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
            }

            @Override
            public void onFailure(int reason) {

            }
        });
    }

    @Override
    public void onHostDestroy(){

    }

    @ReactMethod
    public void initWifiDirect(){ // Opens the WiFi settings
        Activity currentActivity = getCurrentActivity();

        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
        currentActivity.startActivity(intent);
    }
    // public void startRegistration(String serviceType, Integer port,  String name, String socketname, String socketport) {

    @ReactMethod
    public void startRegistration(String buddyname, String serviceType){
        //  Create a string map containing information about your service.
        WifiDirectSocketAsyncTask serverSocket = new WifiDirectSocketAsyncTask(context, 4000);
        serverSocket.execute();
        Map record = new HashMap();
        record.put("listenport", String.valueOf(port));
        buddyname = buddyname + (int) (Math.random() * 1000);
        record.put("buddyname", buddyname);
        record.put("servicetype", serviceType);
        record.put("available", "visible");

        // Service information.  Pass it an instance name, service type
        // _protocol._transportlayer , and the map containing
        // information other devices will want once they connect to this one.
        final WifiP2pDnsSdServiceInfo serviceInfo =
                WifiP2pDnsSdServiceInfo.newInstance(buddyname, serviceType, record);

        // Add the local service, sending the service info, network channel,
        // and listener that will be used to indicate success or failure of
        // the request.
        RegisteredServices.put(buddyname, serviceInfo);
        wifiP2pManager.addLocalService(wifiDirectChannel , serviceInfo, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                // Command successful! Code isn't necessarily needed here,
                // Unless you want to update the UI or add logging statements.
                Toast.makeText(context, "Service Registered", duration).show();

            }

            @Override
            public void onFailure(int arg0) {
                // Command failed.  Check for P2P_UNSUPPORTED, ERROR, or BUSY
            }
        });
    }

    @ReactMethod
    public void deleteRegistration(String name){
        WifiP2pDnsSdServiceInfo serviceInfo = RegisteredServices.get(name);
        wifiP2pManager.removeLocalService(wifiDirectChannel, serviceInfo, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onFailure(int reason) {

            }
        });
    }



    @ReactMethod
    public void discoverPeers(){ // This starts the discovery, real discovery happens in PeerListListener
        wifiP2pManager.discoverPeers(wifiDirectChannel,new WifiP2pManager.ActionListener(){
            @Override
            public void onSuccess() {
            }

            @Override
            public void onFailure(int reason) {
            }
        });
    }

    private WifiP2pManager.PeerListListener mPeerListListener = new WifiP2pManager.PeerListListener() {
        @Override
        public void onPeersAvailable(WifiP2pDeviceList peersList) {
            Collection<WifiP2pDevice> aList = peersList.getDeviceList();
            Object[] arr = aList.toArray();
            for (int i = 0; i < arr.length; i++) {
                WifiP2pDevice a = (WifiP2pDevice) arr[i];
                if (!devices.contains(a)) {
                    devices.add(a);
                    WritableMap params = Arguments.createMap();
                    params.putString("Address", a.deviceAddress);
                    params.putString("Device Name", a.deviceName);
                    sendEvent("onWifiDirectPeers", params);
                }
            }
        }
    };



    final HashMap<String, String> buddies = new HashMap<String, String>();


    @ReactMethod
    private void discoverServices() {

        WifiP2pManager.DnsSdTxtRecordListener txtListener = new WifiP2pManager.DnsSdTxtRecordListener() {
            @Override
            /* Callback includes:
             * fullDomain: full domain name: e.g "printer._ipp._tcp.local"
             * record: TXT record dta as a map of key/value pairs.
             * device: The device running the advertised service.
             */

            public void onDnsSdTxtRecordAvailable(
                    String fullDomain, Map record, WifiP2pDevice device) {
                Log.d(TAG, "DnsSdTxtRecord available -" + record.toString());
                buddies.put(device.deviceAddress, (String) record.get("buddyname"));
            }
        };

        WifiP2pManager.DnsSdServiceResponseListener servListener = new WifiP2pManager.DnsSdServiceResponseListener() {
            @Override
            public void onDnsSdServiceAvailable(String instanceName, String registrationType,
                                                WifiP2pDevice resourceType) {

                // Update the device name with the human-friendly version from
                // the DnsTxtRecord, assuming one arrived.
                resourceType.deviceName = buddies
                        .containsKey(resourceType.deviceAddress) ? buddies
                        .get(resourceType.deviceAddress) : resourceType.deviceName;



                if (!services.contains(resourceType)) {
                    WritableMap serviceParams = Arguments.createMap();
                    serviceParams.putString("MacAddress", resourceType.deviceAddress);
                    serviceParams.putString("Name", resourceType.deviceName);
                    serviceParams.putString("ServiceType", registrationType);
                    serviceParams.putString("action", "added");
                    services.add(resourceType);
                    sendEvent("onWifiDirectServices", serviceParams);
                }
                CharSequence text = "found service";
                Toast.makeText(context, text, duration).show();
                Log.d(TAG, "onBonjourServiceAvailable " + resourceType + instanceName);
            }
        };

        wifiP2pManager.setDnsSdResponseListeners(wifiDirectChannel, servListener, txtListener);

        serviceRequest = WifiP2pDnsSdServiceRequest.newInstance();
        wifiP2pManager.addServiceRequest(wifiDirectChannel, serviceRequest,
                new WifiP2pManager.ActionListener() {

                    @Override
                    public void onSuccess() {
                        Log.v(TAG,"Added service discovery request");
                    }

                    @Override
                    public void onFailure(int arg0) {
                        Log.v(TAG,"Failed adding service discovery request");
                    }
                });
        wifiP2pManager.discoverServices(wifiDirectChannel, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                Log.v(TAG,"Service discovery initiated");
            }

            @Override
            public void onFailure(int arg0) {
                Log.v(TAG,"Service discovery failed");

            }
        });
    }

    @ReactMethod
    public void wifiDirectConnectToPeer(String address){
        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = address;
        wifiP2pManager.connect(wifiDirectChannel,config,new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {

            }

            @Override
            public void onFailure(int reason) {


            }
        });
    }


    @ReactMethod
    public void wifiDirectSendData(String host, String data){
        Socket socket = new Socket();
        try {
            socket.setReuseAddress(true);
            int port = 4000;
            try{
                socket.bind(null);
                socket.connect((new InetSocketAddress(host, port)), 500);
                OutputStream stream = socket.getOutputStream();
                ObjectOutputStream objstream = new ObjectOutputStream(stream);
                objstream.writeObject(data);
            }
            catch (IOException e){
                Log.e(TAG, e.getMessage());

            }
        }
        catch (SocketException e){
            Log.e(TAG, e.getMessage());
        }

    }
    // All code related to the sockets


    int len;
    Socket socket = new Socket();
    byte buf[]  = new byte[1024];
    boolean socketEnabled = false;




    @ReactMethod
    public void sendData(String str){
        try {
            OutputStream outputStream = socket.getOutputStream();
            ContentResolver cr = context.getContentResolver();
            InputStream inputstream = null;
            inputstream = new ByteArrayInputStream(str.getBytes());
            while ((len = inputstream.read(buf)) != -1) {
                outputStream.write(buf, 0, len);
            }
            outputStream.close();
            inputstream.close();
        }
        catch (IOException e){
            Log.e(TAG, e.getMessage());
        }




    }

    @ReactMethod
    public void setupSocket(String host, int socketport) {
        try {
            socket.bind(null);
            socket.connect((new InetSocketAddress(host, socketport)), 500);
            Toast.makeText(context, "Connected", Toast.LENGTH_SHORT).show();
            socketEnabled = true;
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            socketEnabled = false;
        }
    }

    @ReactMethod
    public void closeSocket(){
        try {
            socket.close();
            socketEnabled = false;
        }
        catch (IOException e) {
            Log.e(TAG, e.getMessage());
            socketEnabled = false;
        }

    }

    public void makeToast(CharSequence sequence){
        Toast.makeText(context, sequence, Toast.LENGTH_SHORT).show();

    }

}