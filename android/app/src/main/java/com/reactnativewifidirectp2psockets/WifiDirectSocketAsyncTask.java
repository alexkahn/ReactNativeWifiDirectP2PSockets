package com.reactnativewifidirectp2psockets;

import android.content.Context;
import android.net.wifi.p2p.WifiP2pInfo;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;


import android.widget.Toast;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;


import okio.Utf8;
import org.apache.commons.io.IOUtils;

import static android.content.ContentValues.TAG;


public class WifiDirectSocketAsyncTask extends AsyncTask<Void, Void, String>{

    private  WiFiDirectModule wifidirect;
    private ReactApplicationContext context;
    private TextView statusText;
    private ServerSocket serverSocket;
    private  WifiP2pInfo groupInfo;
    private boolean isGroupOwner;
    private int port;

    private int msgCounter = 0;
    private String clientIP;


    public WifiDirectSocketAsyncTask(ReactApplicationContext context, int port) {
        this.context = context;
//        this.groupInfo = groupInfo;
//        this.isGroupOwner = groupInfo.isGroupOwner;
        this.port = port;
    }

    @Override
    protected String doInBackground(Void... params) {
            try {
                serverSocket = new ServerSocket(port);
//                serverSocket.setReuseAddress(true);
                String res;
                Log.d(TAG, "Server: Socket opened");
//                Toast.makeText(context, "Server: Socket opened", Toast.LENGTH_LONG).show();
//                wifidirect.makeToast("Server Socket open");
                Socket client = serverSocket.accept();
                Log.d(TAG, "Server: connection done");
//                wifidirect.makeToast("Connection done");
//                Toast.makeText(context, "Server: connection done", Toast.LENGTH_SHORT).show();
                clientIP = client.getInetAddress().getHostAddress();
                // Inputstream to string
                // string to JS

                ObjectInputStream objectInputStream = new ObjectInputStream(client.getInputStream());
                try {
                    Object object = objectInputStream.readObject();
                    if (msgCounter == 0) {
                        msgCounter++;
                        res = clientIP;
                    }
                    else if (object.getClass().equals(String.class)){
                        String obj = object.toString();
                        res = obj;
                    }
                    else {
                        res = null;
                    }
                    return res;
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    return null;
                }

            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
                return null;
            }
    }
    @Override
    protected void onPostExecute(String result) {
        if (result != null) {
            // Only the device that is not GroupOwner can send a message to the other
            // A first message with the IP-address will be send to the GO
            // This has to be handled differently
                WritableMap groupOwnerParams = Arguments.createMap();
                groupOwnerParams.putBoolean("groupOwner", groupInfo.isGroupOwner);
                groupOwnerParams.putString("otherDevicesAddress", clientIP);
//                wifidirect.makeToast(txt);
//                sendEvent("onIfGroupOwner", groupOwnerParams);
            } else {
                // Send the string back to the React Native code
                // JavaScript is better in parsing JSON
                WritableMap params = Arguments.createMap();
                params.putString("result", result);
//                sendEvent("OnMessageReceived", params);
            }
        }

    public void closeServerSocket() {
        try {
            serverSocket.close();
        }
        catch (IOException e){
            Log.e("ServerSocket", e.getMessage());
        }
    }
    private String getDottedDecimalIP(byte[] ipAddr) {
        //convert to dotted decimal notation:
        String ipAddrStr = "";
        for (int i=0; i<ipAddr.length; i++) {
            if (i > 0) {
                ipAddrStr += ".";
            }
            ipAddrStr += ipAddr[i]&0xFF;
        }
        return ipAddrStr;
    }
    public void sendEvent(String eventName, @Nullable WritableMap params){
        context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName,params);
    }
}
