import { NativeModules, DeviceEventEmitter } from 'react-native';


export default class SocketModule {

    SocketMod = NativeModules.SocketModule;


    constructor (props) {

        this.addDeviceListeners() 
    }

    _dListeners = {};


    addDeviceListeners (){
        if (Object.keys(this._dListeners).length){
            return this.emit('error', new Error("Socket listeners are already in place"))
        }
        this._dListeners.SocketSetted = DeviceEventEmitter.addListener('SocketSetted', params => {
            console.log(params)
        }); 
   
    }

    /**
     * Remove all event listeners and clean map
     */
    removeDeviceListeners () {
        Object.keys(this._dListeners).forEach(name => this._dListeners[name].remove())
        this._dListeners = {}
    }

    setupSocket(address, port) {
        NativeModules.SocketModule.setupSocket(address, port);
    }

    startThread() {
        NativeModules.SocketModule.startThread();
    }

    setMessage(message) {
        NativeModules.SocketModule.setMessage(message);
    }

}
